package com.ureason.app.asset.grpc.report;

import com.ureason.app.asset.grpc.proto.Report;

import java.util.ArrayList;
import java.util.List;

public class DemoReport {

    static List<Report.ReportProperty> props = new ArrayList<>();

    static {
        props.add(createReportProperty("StictionIssue", 2, ""));
        props.add(createReportProperty("OvershootIssue", 0, ""));
        props.add(createReportProperty("UndershootIssue", 0, ""));
        props.add(createReportProperty("UndershootIssue", 2, ""));
        props.add(createReportProperty("PackingBellowsWearIssue", 2, ""));
        props.add(createReportProperty("StuckIssue", 2, ""));
        props.add(createReportProperty("SpringDiaphragmBrokenIssue", 2, ""));
        props.add(createReportProperty("UnstableControlPerformanceIssue", 0, ""));
        props.add(createReportProperty("IncreasingDeadBandIssue", 0, ""));
        props.add(createReportProperty("PositionerDriftIssue", 2, ""));
        props.add(createReportProperty("PositionerCalibrationIssue", 2, ""));
        props.add(createReportProperty("TotalRiseTime", 115625, ""));
        props.add(createReportProperty("TotalStaleTime", 106080, ""));
        props.add(createReportProperty("OverShootPercent", 0, "%"));
        props.add(createReportProperty("UndershootPercent", 10.73, "%"));
        props.add(createReportProperty("StrokesTotalNumber", 5823, ""));
        props.add(createReportProperty("StrokesRising", 2390, ""));
        props.add(createReportProperty("StrokesFalling", 3433, ""));
        props.add(createReportProperty("Stictionindicator", 0.53, ""));
        props.add(createReportProperty("HealthScore", 15, ""));
        props.add(createReportProperty("TotalMovementOfValve", 0, ""));
        props.add(createReportProperty("TotalEnergyLoss", 0, "kWh"));
        props.add(createReportProperty("LastUpdates", 2, ""));

    }

    public static Report.AssetReport createAssetReport(String assetId, String site) {
        return Report.AssetReport.newBuilder().addAllReportProperties(props).setAssetId(assetId).setSite(site).build();
    }

    public static Report.ReportProperty createReportProperty(String name, Object value, String unit) {
        Report.ReportProperty.Builder reportProperty = Report.ReportProperty.newBuilder().
                setName(name).
                setUnit(unit);

        if (value instanceof Double) {
            reportProperty.setDbl((Double)value);
        } else if (value instanceof Integer) {
            reportProperty.setInt((Integer)value);
        } else if (value instanceof String) {
            reportProperty.setStr((String)value);
        } else if (value instanceof Boolean) {
            reportProperty.setBool((Boolean)value);
        }

        return reportProperty.build();
    }


}
