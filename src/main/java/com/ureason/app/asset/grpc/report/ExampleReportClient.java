package com.ureason.app.asset.grpc.report;

import com.ureason.app.asset.grpc.proto.AssetReportServiceGrpc;
import com.ureason.app.asset.grpc.proto.Report;
import io.grpc.Channel;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;

import java.util.Iterator;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

/**
 * Example ReportClient using GRPC Report Service client stubs
 */
public class ExampleReportClient {
    private static final Logger logger =
            Logger.getLogger(ExampleReportClient.class.getName());

    private final AssetReportServiceGrpc.AssetReportServiceBlockingStub blockingStub;

    public ExampleReportClient(Channel channel) {
        blockingStub = AssetReportServiceGrpc.newBlockingStub(channel);
    }

    private void getReports(String site) {
        try {
            Iterator<Report.AssetReport> reports = blockingStub.getReportData(createQuery(site));
            for (int i = 1; reports.hasNext(); i++) {
                Report.AssetReport report = reports.next();
                logger.info("Report #" + i + " Site =" + report.getSite() + " AssetId=" +  report.getAssetId());
            }
        } catch (StatusRuntimeException e) {
            logger.warning("RPC failed: " + e.getStatus());
        }
    }

    public static void main(String[] args) throws InterruptedException {

        // Create a channel and a stub
        ManagedChannel channel = ManagedChannelBuilder
                .forAddress("localhost", 50052)
                .usePlaintext()
                .build();

        ExampleReportClient reportClient = new ExampleReportClient(channel);
        reportClient.getReports("Site1");
        reportClient.getReports("Site2");

        channel.shutdown();
        channel.awaitTermination(1, TimeUnit.SECONDS);

    }

    public static Report.AssetReportQuery createQuery(String site) {
        return Report.AssetReportQuery.newBuilder().setSite(site).build();
    }
}
