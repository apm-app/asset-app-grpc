package com.ureason.app.asset.grpc.ingest;

import com.google.protobuf.Timestamp;
import com.ureason.app.asset.grpc.proto.AssetIngestionServiceGrpc;
import com.ureason.app.asset.grpc.proto.Ingest;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.ClientCallStreamObserver;
import io.grpc.stub.ClientResponseObserver;


import java.time.Instant;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

/**
 * Example Ingestion Client using Asset Ingestion GRPC Service client stubs
 */
public class ExampleIngestionClient {
    private static final Logger logger =
            Logger.getLogger(ExampleIngestionClient.class.getName());

    public static void main(String[] args) throws InterruptedException {
        final CountDownLatch done = new CountDownLatch(1);

        // Create a channel and a stub
        ManagedChannel channel = ManagedChannelBuilder
                .forAddress("localhost", 50051)
                .usePlaintext()
                .build();

        AssetIngestionServiceGrpc.AssetIngestionServiceBlockingStub blockingStub = AssetIngestionServiceGrpc.newBlockingStub(channel);
        // Fetch Asset and tag info for Site1
        Ingest.AssetDetails assetDetails = blockingStub.getAssetDetails(Ingest.AssetQuery.newBuilder().setSite("Site1").build());
        List<Ingest.IngestDataRequest> requests = new ArrayList<>();
        for (Ingest.AssetDataInfo assetDataInfo : assetDetails.getAssetDataInfoList()) {

            if (assetDataInfo.getLastDataPointDatasReceivedCount() > 0) {
                // Access last Data Point ingested
                for (Ingest.DataPointData dataPointData : assetDataInfo.getLastDataPointDatasReceivedList()) {
                    logger.info("Last DataPoint Ingested for " +
                            assetDataInfo.getAssetId() + "." + dataPointData.getTagId() + " was " +
                            Instant.ofEpochMilli(com.google.protobuf.util.Timestamps.toMillis(
                                    dataPointData.getData(0).getTimestamp())).toString());
                }
            }

            // create some data for data requests
            requests.add(createDataRequest(
                    assetDataInfo.getAssetId(),
                    assetDataInfo.getTagIdsList().toArray(new String[0]),
                    50000));
        }
        //List<Ingest.IngestDataRequest> requests = createRequests(100);

        // Create an Asynch stub for streaming access
        AssetIngestionServiceGrpc.AssetIngestionServiceStub stub = AssetIngestionServiceGrpc.newStub(channel);

        // When using manual flow-control and back-pressure on the client, the ClientResponseObserver handles both
        // request and response streams.
        ClientResponseObserver<Ingest.IngestDataRequest, Ingest.IngestionResponse> clientResponseObserver =
                new ClientResponseObserver<Ingest.IngestDataRequest, Ingest.IngestionResponse>() {

                    ClientCallStreamObserver<Ingest.IngestDataRequest> requestStream;
                    List<Ingest.IngestDataRequest> requestsAccepted = new ArrayList<>();

                    @Override
                    public void beforeStart(final ClientCallStreamObserver<Ingest.IngestDataRequest> requestStream) {
                        this.requestStream = requestStream;
                        // Set up manual flow control for the response stream.
                        requestStream.disableAutoRequestWithInitial(1);

                        // Set up a back-pressure-aware producer for the request stream. The onReadyHandler will be invoked
                        // when the consuming side has enough buffer space to receive more messages.
                        //
                        // Messages are serialized into a transport-specific transmit buffer. Depending on the size of this buffer,
                        // MANY messages may be buffered, however, they haven't yet been sent to the server. The server must call
                        // request() to pull a buffered message from the client.
                        //
                        // Note: the onReadyHandler's invocation is serialized on the same thread pool as the incoming
                        // StreamObserver's onNext(), onError(), and onComplete() handlers. Blocking the onReadyHandler will prevent
                        // additional messages from being processed by the incoming StreamObserver. The onReadyHandler must return
                        // in a timely manner or else message processing throughput will suffer.
                        requestStream.setOnReadyHandler(new Runnable() {
                            // An iterator is used so we can pause and resume iteration of the request data.
                            Iterator<Ingest.IngestDataRequest> iterator = requests.iterator();

                            @Override
                            public void run() {
                                // Start generating values from where we left off on a non-gRPC thread.
                                while (requestStream.isReady()) {
                                    if (iterator.hasNext()) {
                                        // Send more messages if there are more messages to send.
                                        Ingest.IngestDataRequest request = iterator.next();
                                        logger.info("Sending --> " + request.getAssetId());
                                        requestStream.onNext(request);
                                    } else {
                                        // Signal completion if there is nothing left to send.
                                        requestStream.onCompleted();
                                    }
                                }
                            }
                        });
                    }

                    @Override
                    public void onNext(Ingest.IngestionResponse response) {
                        logger.info("Server Status Received " + response.getStatus() + " for " + response.getAssetId());
                        // Signal the sender to send one message.
                        requestStream.request(1);
                    }

                    @Override
                    public void onError(Throwable t) {
                        t.printStackTrace();
                        done.countDown();
                    }

                    @Override
                    public void onCompleted() {
                        logger.info("All Done");
                        done.countDown();
                    }
                };

        // Note: clientResponseObserver is handling both request and response stream processing.
        stub.ingestFromStream(clientResponseObserver);

        done.await();

        channel.shutdown();
        channel.awaitTermination(1, TimeUnit.SECONDS);
    }


    public static Ingest.IngestDataRequest createDataRequest(
            String assetId,
            String[] tagIds,
            int dataCount) {

        Ingest.IngestDataRequest.Builder request = Ingest.IngestDataRequest.newBuilder();

        for (String tagId : tagIds) {
            request.addDataPointData(
                    createDataPointData(tagId, dataCount));
        }

        return request.setAssetId(assetId).build();


    }

    public static Ingest.DataPointData createDataPointData(String tagId, int count) {
        Ingest.DataPointData.Builder dataPointData = Ingest.DataPointData.newBuilder();
        for (int i = 0; i<count; i++) {
            //String timestamp = ZonedDateTime.now( ZoneOffset.UTC ).format( DateTimeFormatter.ISO_INSTANT );
            long millis = System.currentTimeMillis();
            Timestamp timestamp = Timestamp.newBuilder().setSeconds(millis / 1000)
                    .setNanos((int) ((millis % 1000) * 1000000)).build();
            double value = Math.random()*100;
            Ingest.Data data = Ingest.Data.newBuilder().
                    setTimestamp(timestamp).
                    setValue(value).build();
            dataPointData.addData(data);
        }
        return dataPointData.setTagId(tagId).build();
    }
}
