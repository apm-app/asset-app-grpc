package com.ureason.app.asset.grpc.report;
import com.ureason.app.asset.grpc.proto.AssetReportServiceGrpc;
import com.ureason.app.asset.grpc.proto.Report;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.stub.StreamObserver;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

/**
 * Example standalone ReportServer implementing the Asset Report GRPC Service
 */
public class ExampleReportServer {
    private static final Logger logger =
            Logger.getLogger(ExampleReportServer.class.getName());

    public static void main(String[] args) throws InterruptedException, IOException {

        AssetReportServiceGrpc.AssetReportServiceImplBase svc = new AssetReportServiceGrpc.AssetReportServiceImplBase() {

            // Service class implementation
            @Override
            public void getReportData(Report.AssetReportQuery request, StreamObserver<Report.AssetReport> responseObserver) {
                // Send a response.
                Report.AssetReport reply1 = DemoReport.createAssetReport("Asset1", request.getSite());
                Report.AssetReport reply2 = DemoReport.createAssetReport("Asset2", request.getSite());
                responseObserver.onNext(reply1);
                responseObserver.onNext(reply2);
                responseObserver.onCompleted();
            }
        };

        final Server server = ServerBuilder
                .forPort(50052)
                .addService(svc)
                .build()
                .start();

        logger.info("Listening on " + server.getPort());

        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                // Use stderr here since the logger may have been reset by its JVM shutdown hook.
                System.err.println("Shutting down");
                try {
                    server.shutdown().awaitTermination(30, TimeUnit.SECONDS);
                } catch (InterruptedException e) {
                    e.printStackTrace(System.err);
                }
            }
        });
        server.awaitTermination();
    }

}