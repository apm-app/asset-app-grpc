package com.ureason.app.asset.grpc.ingest;
import com.ureason.app.asset.grpc.proto.AssetIngestionServiceGrpc;
import com.ureason.app.asset.grpc.proto.Ingest;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.Status;
import io.grpc.stub.ServerCallStreamObserver;
import io.grpc.stub.StreamObserver;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

/**
 * Example standalone IngestionServer implementing the Asset Ingestion GRPC Service
 */
public class ExampleIngestionServer {
    private static final Logger logger =
            Logger.getLogger(ExampleIngestionServer.class.getName());

    static Map<String, String[]> mAssetBase = new HashMap<>();
    static {
        // Create simple test AssetDB
        for (int i = 1; i<50; i++) {
            String assetName = "Asset"+i;
            mAssetBase.put(assetName, new String[] {
                    assetName+"_Tag1", assetName+"_Tag2"
            });
        }

    }
    static Map<String, Map<String, Ingest.DataPointData>> mLastDataPointsLookup = new HashMap<>();

    static String ingestorId = UUID.randomUUID().toString();

    public static void main(String[] args) throws InterruptedException, IOException {
        // Service class implementation
        AssetIngestionServiceGrpc.AssetIngestionServiceImplBase svc = new AssetIngestionServiceGrpc.AssetIngestionServiceImplBase() {

            @Override
            public StreamObserver<Ingest.IngestDataRequest> ingestFromStream(StreamObserver<Ingest.IngestionResponse> responseObserver) {
                // Set up manual flow control for the request stream.
                final ServerCallStreamObserver<Ingest.IngestionResponse> serverCallStreamObserver =
                        (ServerCallStreamObserver<Ingest.IngestionResponse>) responseObserver;
                serverCallStreamObserver.disableAutoRequest();

                // Set up a back-pressure-aware consumer for the request stream. The onReadyHandler will be invoked
                // when the consuming side has enough buffer space to receive more messages.
                //
                // Note: the onReadyHandler's invocation is serialized on the same thread pool as the incoming StreamObserver's
                // onNext(), onError(), and onComplete() handlers. Blocking the onReadyHandler will prevent additional messages
                // from being processed by the incoming StreamObserver. The onReadyHandler must return in a timely manner or
                // else message processing throughput will suffer.
                class OnReadyHandler implements Runnable {
                    // Guard against spurious onReady() calls caused by a race between onNext() and onReady(). If the transport
                    // toggles isReady() from false to true while onNext() is executing, but before onNext() checks isReady(),
                    // request(1) would be called twice - once by onNext() and once by the onReady() scheduled during onNext()'s
                    // execution.
                    private boolean wasReady = false;

                    @Override
                    public void run() {
                        if (serverCallStreamObserver.isReady() && !wasReady) {
                            wasReady = true;
                            logger.info("READY");
                            // Signal the request sender to send one message. This happens when isReady() turns true, signaling that
                            // the receive buffer has enough free space to receive more messages. Calling request() serves to prime
                            // the message pump.
                            serverCallStreamObserver.request(1);
                        }
                    }
                }
                final OnReadyHandler onReadyHandler = new OnReadyHandler();
                serverCallStreamObserver.setOnReadyHandler(onReadyHandler);

                // Give gRPC a StreamObserver that can observe and process incoming requests.
                return new StreamObserver<Ingest.IngestDataRequest>() {
                    @Override
                    public void onNext(Ingest.IngestDataRequest request) {
                        // Process the request and send a response or an error.
                        try {
                            // Accept and enqueue the request.
                            String assetId = request.getAssetId();
                            logger.info("--> " + assetId);
                            List<Ingest.DataPointData> dataPointDatas = request.getDataPointDataList();
                            Map<String, Ingest.DataPointData> lastDataPointDataMap = mLastDataPointsLookup.get(assetId);
                            if (lastDataPointDataMap == null) {
                                lastDataPointDataMap = new HashMap<>();
                                mLastDataPointsLookup.put(assetId, lastDataPointDataMap);
                            }
                            if (dataPointDatas != null) {
                                for (Ingest.DataPointData dataPointData : dataPointDatas) {
                                    logger.info("TagId : " + dataPointData.getTagId() + "," + dataPointData.getDataCount());
                                    // Record last DataPoint received for this Asset.tag
                                    if (dataPointData.getDataCount() > 0) {
                                        Ingest.DataPointData lastDataPoint =
                                                Ingest.DataPointData.newBuilder().
                                                        setTagId(dataPointData.getTagId()).
                                                        addData(dataPointData.getData(dataPointData.getDataCount() - 1)).build();
                                        lastDataPointDataMap.put(dataPointData.getTagId(), lastDataPoint);
                                    }
                                }
                            }

                            // Simulate server "work"
                            //Thread.sleep(100);

                            // Send a response.
                            Ingest.IngestionResponse reply =
                                    Ingest.IngestionResponse.newBuilder().
                                            setIngestorId(ingestorId).
                                            setStatus(Ingest.IngestionStatus.READY).
                                            setAssetId(assetId).
                                            build();
                            responseObserver.onNext(reply);

                            // Check the provided ServerCallStreamObserver to see if it is still ready to accept more messages.
                            if (serverCallStreamObserver.isReady()) {
                                // Signal the sender to send another request. As long as isReady() stays true, the server will keep
                                // cycling through the loop of onNext() -> request(1)...onNext() -> request(1)... until the client runs
                                // out of messages and ends the loop (via onCompleted()).
                                //
                                // If request() was called here with the argument of more than 1, the server might runs out of receive
                                // buffer space, and isReady() will turn false. When the receive buffer has sufficiently drained,
                                // isReady() will turn true, and the serverCallStreamObserver's onReadyHandler will be called to restart
                                // the message pump.
                                serverCallStreamObserver.request(1);
                            } else {
                                // If not, note that back-pressure has begun.
                                onReadyHandler.wasReady = false;
                            }
                        } catch (Throwable throwable) {
                            throwable.printStackTrace();
                            responseObserver.onError(
                                    Status.UNKNOWN.withDescription("Error handling request").withCause(throwable).asException());
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        // End the response stream if the client presents an error.
                        t.printStackTrace();
                        responseObserver.onCompleted();
                    }

                    @Override
                    public void onCompleted() {
                        // Signal the end of work when the client ends the request stream.
                        logger.info("COMPLETED");
                        responseObserver.onCompleted();
                    }
                };

            }

            @Override
            public void getAssetDetails(Ingest.AssetQuery request, StreamObserver<Ingest.AssetDetails> responseObserver) {

                // Return AssetDetails from test AssetDB aswell as any last data points that may have been received previously

                Ingest.AssetDetails.Builder assetDetailsB = Ingest.AssetDetails.newBuilder();
                for (Map.Entry<String, String[]> mAssetDBEntry : mAssetBase.entrySet()) {
                    String assetName = mAssetDBEntry.getKey();
                    Map<String, Ingest.DataPointData> lastDataPointsForAsset = mLastDataPointsLookup.get(assetName);

                    Ingest.AssetDataInfo.Builder assetDataInfoB = Ingest.AssetDataInfo.newBuilder().
                            setAssetId(assetName).
                            addAllTagIds(Arrays.asList(mAssetDBEntry.getValue()));

                    if (lastDataPointsForAsset != null) {
                        assetDataInfoB.addAllLastDataPointDatasReceived(lastDataPointsForAsset.values());
                    }

                    assetDetailsB.addAssetDataInfo(assetDataInfoB.build());
                }

                responseObserver.onNext(assetDetailsB.build());
                responseObserver.onCompleted();
            }

        };

        final Server server = ServerBuilder
                .forPort(50051)
                .addService(svc)
                .build()
                .start();

        logger.info("Listening on " + server.getPort());

        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                // Use stderr here since the logger may have been reset by its JVM shutdown hook.
                System.err.println("Shutting down");
                try {
                    server.shutdown().awaitTermination(30, TimeUnit.SECONDS);
                } catch (InterruptedException e) {
                    e.printStackTrace(System.err);
                }
            }
        });
        server.awaitTermination();
    }
}