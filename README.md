## Grpc Service descriptions for UReason's Asset Apps

This repository defines the grpc service interfaces using Google's grpc protobuf.

**Ingestion Service**

Described in src/proto/com/ureason/app/asset/grpc/proto/ingest.proto.

**Report Service**

Described in src/proto/com/ureason/app/asset/grpc/proto/report.proto.

## Example java servers and clients

**IngestionService** examples src/proto/com/ureason/app/asset/grpc/ingest
**ReportService** examples src/proto/com/ureason/app/asset/grpc/report

Example servers are designed to be lightweight/standalone to ease development of service clients.

## pom.xml

The pom currently supports
- auto creation of java proto stubs during compile phase
- creation of jar (includes examples and proto stubs) during package phase
